'''Helper classes for ServiceNow API interaction'''
from collections import namedtuple
from requests import request
from cloudify.workflows import ctx


class ServiceNowAPIError(Exception):
    '''Service Now API exception'''
    def __init__(self, ex):
        ctx.logger.error('ServiceNowAPIError: [%s] %s' % (
            ex.get('status'),
            ex.get('error', dict).get('detail')
        ))
        Exception.__init__(self, ex)


ServiceNowAuth = namedtuple(
    'ServiceNowAuth',
    ['username', 'password', 'instance',
     'url', 'headers'])


SERVICENOW_AUTH_DEFAULT = {
    'username': 'admin',
    'password': 'admin',
    'instance': 'zefflindemo1'
}


class ServiceNow(object):
    '''ServiceNow API class'''
    def __init__(self,
                 username=SERVICENOW_AUTH_DEFAULT['username'],
                 password=SERVICENOW_AUTH_DEFAULT['password'],
                 instance=SERVICENOW_AUTH_DEFAULT['instance'],):
        self.auth = ServiceNowAuth(
            username=username,
            password=password,
            instance=instance,
            url='https://%s.service-now.com' % instance,
            headers={'Content-Type': 'application/json',
                     'Accept': 'application/json'}
        )
        if not self.auth.username or not self.auth.instance:
            raise RuntimeError('Invalid or missing ServiceNow credentials')

    def _call(self, method, endpoint, data=None, api='v1'):
        '''Makes a ServiceNow API call'''
        abs_url = self.auth.url + '/api/now/%s/table' % (api) + endpoint
        ctx.logger.info('ServiceNow API Call: [%s] %s:%s@%s [%s]' %
                        (
                            method,
                            self.auth.username,
                            self.auth.password,
                            abs_url,
                            data
                        ))
        res = request(
            method,
            abs_url,
            auth=(self.auth.username, self.auth.password),
            headers=self.auth.headers,
            json=data)

        ctx.logger.info('ServiceNow API status code: %s' % res.status_code)
        if res.status_code == 204:
            # Successful return w/ no response data
            return {}
        if res.status_code == 401:
            # Error - unauthorized
            raise ServiceNowAPIError(res.json())
        if res.status_code not in [200, 201]:
            return {}
        else:
            return res.json().get('result')

    def object_create(self, table, data):
        '''Creates an object in a table'''
        return self._call('post', '/%s' % table, data)

    def object_modify(self, table, sys_id, data):
        '''Modifies an existing object in a table'''
        return self._call('put', '/%s/%s' % (table, sys_id), data)

    def object_delete(self, table, sys_id):
        '''Deletes an existing object in a table'''
        return self._call('delete', '/%s/%s' % (table, sys_id))

    def object_retrieve(self, table, sys_id):
        '''Retrieves an object in a table'''
        return self._call('get', '/%s/%s' % (table, sys_id))

    def object_find(self, table, query):
        '''Finds objects in a table based on a query string'''
        return self._call('get', '/%s?sysparm_query=%s' % (table, query))

    def object_find_one(self, table, query):
        '''Finds the first object from a table based on a query string'''
        obj = self.object_find(table, query)
        if obj and not isinstance(obj, basestring):
            return obj[0]
        return None

    def object_find_by_bundle(self, bundle):
        '''Finds the first object specified by an input bundle'''
        if bundle and bundle.get('table') and \
           bundle.get('type') and bundle.get('value'):
            return self.object_find_one(
                bundle['table'],
                '%s=%s' % (bundle['type'], bundle['value'])
            )
        return None

    def table_schema_retrieve(self, table):
        '''Retrieves a table schema object'''
        return self._call('get', '/schema/%s' % table, api='doc')


class ServiceNowObject(object):
    '''ServiceNow object interface'''
    def __init__(self, conn, table, sys_id=None):
        self.conn = conn
        self.table = table
        self.sys_id = sys_id

    def create(self, data):
        '''Creates an object'''
        obj = self.conn.object_create(self.table, data)
        self.sys_id = obj.get('sys_id')
        return obj

    def modify(self, data):
        '''Modifies an object'''
        return self.conn.object_modify(self.table, self.sys_id, data)

    def delete(self):
        '''Deletes an object'''
        return self.conn.object_delete(self.table, self.sys_id)

    def retrieve(self):
        '''Retrieves an object'''
        return self.conn.object_retrieve(self.table, self.sys_id)
