'''Custom workflows for ServiceNow integration'''
from .servicenow import (ServiceNow, ServiceNowObject)
from cloudify.decorators import workflow
from cloudify.workflows import ctx
from cloudify.exceptions import NonRecoverableError
from cloudify.plugins import lifecycle
from cloudify.manager import (get_node_instance, update_node_instance)
from string import Template
from copy import deepcopy

RUNTIME_TABLE = 'servicenow_table'
RUNTIME_SYSID = 'servicenow_sys_id'

SN_INSTANCE = 'zefflindemo1'
SN_USERNAME = 'admin'
SN_PASSWORD = 'admin'

CMDB_REL_QUEUE = []


def get_servicenow_connection(auth):
    '''Gets a new handle to a ServiceNow API connection'''
    try:
        auth = auth or dict()
        return ServiceNow(
            auth.get('username'),
            auth.get('password'),
            auth.get('instance')
        )
    except RuntimeError as ex:
        ctx.logger.error('ServiceNow RuntimeError: %s' % ex)
        raise NonRecoverableError(ex)
    return None


def update_params(params, updates):
    '''Merges updates into params'''
    params = params.copy() if isinstance(params, dict) else dict()
    params.update(updates)
    return params


def config_tmpl(config, val, params=None):
    '''Substitutes templated string objects with values'''
    ctx.logger.info('config_tmpl(%s, %s)' % (val, params))
    if isinstance(val, basestring):
        tmpl = Template(val)
        return tmpl.safe_substitute(update_params(params, {
            'workflow': config.get('workflow'),
            'deployment_id': config.get('deployment_id'),
            'blueprint_id': config.get('blueprint_id'),
            'manager_sys_id': config.get('manager_sys_id'),
            'deployment_sys_id': config.get('deployment_sys_id')
        }))
    return val


def prepare_custom_inputs(snc, config, props, table, subs=None):
    '''Prepares user inputs (incident/CR) for use'''
    ctx.logger.info('Preparing custom user inputs for use')
    schema = snc.table_schema_retrieve(table)
    # Iterate over all user-defined incident properties
    data = dict()
    for key, val in props.iteritems():
        node = [x for x in schema if x['name'] == key]
        node = node[0] if node else None
        if not node:
            continue
        # Most inputs are basic pass-through strings (start here)
        if isinstance(val, basestring):
            dstr = config_tmpl(config, val, subs)
            if dstr:
                data[key] = dstr
        # Special case for reference types (with bundle dict input)
        elif node['internalType'] == 'reference':
            if isinstance(val, dict):
                obj = snc.object_find_by_bundle(val)
                if obj and obj.get('sys_id'):
                    data[key] = obj['sys_id']
        # Default case (pure pass-through, no processing)
        else:
            data[key] = val
    return data


def create_incident_report(snc, config, exc):
    '''Opens a ServiceNow Incident report'''
    ctx.logger.info('Opening ServiceNow Incident Record')
    table = 'incident'
    incident = ServiceNowObject(snc, table)
    # Create the incident report
    incident.create(prepare_custom_inputs(
        snc, config,
        config.get(table, dict),
        table,
        subs={'exception_msg': str(exc)}
    ))


def open_change_request(snc, chreq, config):
    '''Opens a ServiceNow Change Request'''
    ctx.logger.info('Opening ServiceNow Change Request')
    table = 'change_request'
    x_props = config.get(table, dict)
    props = deepcopy(x_props)
    # Remove the CR close overrides
    props.pop('__success__', None)
    props.pop('__failure__', None)
    # Create the change request
    chreq.create(prepare_custom_inputs(snc, config, props, table))
    return chreq


def close_change_request(snc, chreq, config, success):
    '''Closes a ServiceNow Change Request'''
    ctx.logger.info('Closing ServiceNow Change Request')
    table = 'change_request'
    success_str = '__success__' if success else '__failure__'
    props = config.get(table, dict).get(success_str, dict)
    ctx.logger.info('success_str: %s, props: %s' % (success_str, props))
    ctx.logger.info('config[table]: %s' % config[table])
    # Check if 'state' is set. If not, that's a problem
    # and warrants an incident report
    if not props.get('state'):
        ctx.logger.warn('Could not close Change Request because no "state" '
                        'was provided by the user. Creating Incident Report.')
        create_incident_report(
            snc, config,
            'Cloudify attempted to close a ServiceNow Change Request (%s) but '
            'no "state" input was defined by the user.  Please set the "state" '
            'input for all 3 change request states (global (root), '
            '__success__, __failure__) in your workflow inputs file.'
            % chreq.sys_id)
    chreq.modify(prepare_custom_inputs(snc, config, props, table))


def cmdb_get_manager(snc, config):
    '''Gets a handle to the CMDB Cloudify Manager object'''
    mgr_sysid = config.get('manager', dict()).get('sys_id')
    mgr_table = config.get('manager', dict()).get('table')
    if mgr_sysid and mgr_table:
        config['manager_sys_id'] = mgr_sysid
        return ServiceNowObject(snc, mgr_table, sys_id=mgr_sysid)
    return None


def cmdb_get_deployment(snc, config):
    '''Get a handle to the CMDB deployment object'''
    deployment_table = config['cmdb_node_map']['__deployment__']
    dep = snc.object_find_one(deployment_table,
                              'name=%s' % config['deployment_id'])
    if dep:
        config['deployment_sys_id'] = dep['sys_id']
        return ServiceNowObject(snc,
                                deployment_table,
                                sys_id=dep['sys_id'])
    return None


def cmdb_create_deployment(snc, config):
    '''Create a CMDB deployment object'''
    deployment_table = config['cmdb_node_map']['__deployment__']
    cmdb_dep = ServiceNowObject(snc, deployment_table)
    cmdb_dep.create({'name': config['deployment_id']})
    config['deployment_sys_id'] = cmdb_dep.sys_id
    return cmdb_dep


def cmdb_delete_deployment(snc, config):
    '''Delete a CMDB deployment object'''
    cmdb_dep = cmdb_get_deployment(snc, config)
    ctx.logger.info('Deleting deployment [%s] from ServiceNow CMDB' %
                    cmdb_dep.sys_id)
    cmdb_dep.delete()


def cmdb_process_relationship_queue(snc):
    '''Create a CMDB relationship'''
    for rel in CMDB_REL_QUEUE:
        ctx.logger.info('Creating relationship [%s]: %s <-> %s' % (
            rel['type'], rel['parent'], rel['child']
        ))
        cmdb_rel = ServiceNowObject(snc, 'cmdb_rel_ci')
        cmdb_rel.create(rel)


def cmdb_queue_relationship(snc, config, parent, child, rel_type=None):
    '''Create a CMDB relationship'''
    ctx.logger.info('cmdb_queue_relationship(%s, %s, %s)' % (
        parent, child, rel_type
    ))
    rel_type = snc.object_find_one(
        config['cmdb_node_map']['__relationships__'],
        'name=%s' % (
            rel_type if rel_type else config['cmdb_rel_map']['__default__']
        ))
    ctx.logger.info('Queueing relationship (type=%s) '
                    'between parent (%s) and child (%s)' % (
                        rel_type.get('name'), parent, child
                    ))
    CMDB_REL_QUEUE.append({
        'type': rel_type.get('sys_id'),
        'parent': parent,
        'child': child
    })


def cmdb_find_best_table(config, instance):
    '''Finds the best match for a CMDB table / type'''
    node = instance.node
    table_map = config['cmdb_node_map']
    # Search (from most abstracted to least) to find a CMDB table match
    ctx.logger.info('table_map: %s' % table_map)
    ctx.logger.info('find_table: %s' % node.type_hierarchy)
    for _type in reversed(node.type_hierarchy):
        _table = table_map.get(_type)
        if _table:
            ctx.logger.info('Using CMDB table %s for instance %s' %
                            (_table, instance.id))
            return _table
    # We don't know where to place this instance in the CMDB
    # This only happens when there's no cloudify.nodes.Root entry
    # defined in the configuration file on the manager
    NonRecoverableError('Could not find a suitable CMDB table for instance '
                        '%s (types: %s)' % (instance.id, node.type_hierarchy))


def is_compute_node(node):
    '''Check if the node is derived from cloudify.nodes.Compute'''
    return True if 'cloudify.nodes.Compute' in node.type_hierarchy else False


def is_compute_instance(instance):
    '''Check if the instance is derived from cloudify.nodes.Compute'''
    return is_compute_node(instance.node)


def cmdb_add_node(snc,
                  config,
                  child,
                  data=None):
    '''Adds an instance node to the CMDB

        :param ServiceNow snc: ServiceNow API connection
        :param ServiceNowConfig config: ServiceNow configuration data
        :param CloudifyWorkflowNodeInstance child: Cloudify
        workflow node instance
        :param dict data: Free-form JSON object that will be passed to
        the CMDB as-is (besides the "name" attribute)
        :returns ServiceNowObject: Child CMDB reference
    '''
    ctx.logger.info('Adding node instance %s to the CMDB' % child.id)
    # Find the best-fit CMDB table to use
    cmdb_table = cmdb_find_best_table(config, child)
    if not cmdb_table:
        NonRecoverableError('Can\'t create object %s because there is no '
                            'compatible CMDB table defined in the '
                            'configuration (no default found)' % child.id)
    cmdb_child = ServiceNowObject(snc, cmdb_table)
    # Merge user-provided data with ours
    if not data:
        data = dict()
    m_data = data.copy()
    m_data.update({'name': child.id})
    # Create the CMDB object
    cmdb_child.create(m_data)
    # Update runtime properties with ServiceNow references
    cfy_child = get_node_instance(child.id)
    cfy_child.runtime_properties[RUNTIME_TABLE] = cmdb_table
    cfy_child.runtime_properties[RUNTIME_SYSID] = cmdb_child.sys_id
    update_node_instance(cfy_child)
    return cmdb_child


def cmdb_add_relationship_tree(snc, config, instance, to_dep=False):
    '''Recursive method to add instance relationships to the CMDB

        :param ServiceNow snc: ServiceNow API connection
        :param ServiceNowConfig config: ServiceNow configuration data
        :param CloudifyWorkflowNodeInstance instance: Cloudify
        workflow node instance
        :param boolean to_dep: True if the instance's parent is the
        deployment object
    '''
    # Convert CloudifyWorkflowNodeInstance to CloudifyNodeInstance
    source = get_node_instance(instance.id)

    # If this is a root instance, relate it to the deployment
    if to_dep:
        cmdb_dep = cmdb_get_deployment(snc, config)
        cmdb_queue_relationship(
            snc, config,
            cmdb_dep.sys_id,
            source.runtime_properties[RUNTIME_SYSID]
        )

    # Queue all instance relationships
    rel_map = config['cmdb_rel_map']
    for rel in instance.relationships:
        # pylint: disable=W0212
        rels = rel.relationship._relationship["type_hierarchy"]
        for _rel in reversed(rels):
            rel_type = rel_map.get(_rel)
            if rel_type:
                break
        target = get_node_instance(rel.target_id)
        cmdb_queue_relationship(
            snc, config,
            target.runtime_properties[RUNTIME_SYSID],
            source.runtime_properties[RUNTIME_SYSID],
            rel_type=rel_type
        )

    # Iterate through all children instances and repeat...
    for child in instance.contained_instances:
        cmdb_add_relationship_tree(snc, config, child)


def cmdb_add_relationships(snc, config):
    '''Add nodes to the ServiceNow CMDB'''
    for node in ctx.nodes:
        if 'cloudify.nodes.Compute' in node.type_hierarchy:
            for instance in node.instances:
                cmdb_add_relationship_tree(snc, config, instance, to_dep=True)
    cmdb_process_relationship_queue(snc)


def cmdb_add_node_tree(snc, config, instance):
    '''Adds an entire Cloudify instance tree to ServiceNow from a VM'''
    # Iterate through all children instances and repeat...
    cfy_instance = get_node_instance(instance.id)
    props = instance.node.properties.get('servicenow_props', dict())
    data = dict()
    for key, val in props.iteritems():
        data[key] = cfy_instance.runtime_properties.get(val)

    cmdb_add_node(
        snc, config,
        instance,
        data=data
    )
    for child in instance.contained_instances:
        cmdb_add_node_tree(snc, config, child)


def cmdb_add_nodes(snc, config):
    '''Add nodes to the ServiceNow CMDB'''
    for node in ctx.nodes:
        if 'cloudify.nodes.Compute' in node.type_hierarchy:
            for instance in node.instances:
                cfy_instance = get_node_instance(instance.id)
                ctx.logger.info('%s runtime_props: %s' % (
                    instance.id, cfy_instance.runtime_properties
                ))
                cmdb_add_node_tree(snc, config, instance)


def cmdb_remove_nodes(snc):
    '''Removes all registered nodes from the ServiceNow CMDB'''
    # Iterate through all node instances
    for node in ctx.nodes:
        for instance in node.instances:
            # Get the CMDB table + sys_id for the instance
            cfy_instance = get_node_instance(instance.id)
            cmdb_table = cfy_instance.runtime_properties.get(RUNTIME_TABLE)
            sys_id = cfy_instance.runtime_properties.get(RUNTIME_SYSID)
            if sys_id and cmdb_table:
                ctx.logger.info('Deleting object %s [%s] from table %s' %
                                (instance.id, sys_id, cmdb_table))
                # Query for the CMDB object and purge it (breaks relationships)
                cmdb_vm = ServiceNowObject(snc, cmdb_table, sys_id=sys_id)
                if not cmdb_vm:
                    ctx.logger.warn('Can\'t delete object %s [%s] from table '
                                    '"%s" because it was not found or no '
                                    'longer exists at that location' %
                                    (instance.id, sys_id, cmdb_table))
                else:
                    cmdb_vm.delete()
            else:
                # No table or sys_id was available, warn and continue
                ctx.logger.warn('Can\'t delete object %s [%s] from table "%s" '
                                'because either the sys_id or cmdb_group is '
                                'missing' % (instance.id, sys_id, cmdb_table))


@workflow
def install(config, **_):
    '''ServiceNow wrapper workflow'''
    snc = get_servicenow_connection(config['authentication'])

    config['workflow'] = 'sn_install'
    config['deployment_id'] = ctx.deployment.id
    config['blueprint_id'] = ctx.blueprint.id

    try:
        # Find the Cloudify Manager CMDB entry
        cmdb_mgr = cmdb_get_manager(snc, config)
        if not cmdb_mgr:
            ctx.logger.warn('No CMDB entry was found for the Cloudify manager')
        # Create the Cloudify Deployment CMDB entry
        cmdb_dep = cmdb_create_deployment(snc, config)
        if not cmdb_dep:
            raise RuntimeError('Could not create the CMDB entry for '
                               'the Cloudify Deployment object')
        # (optional) Create relationship between manager & deployment
        if cmdb_mgr and cmdb_dep:
            cmdb_queue_relationship(snc, config,
                                    cmdb_mgr.sys_id,
                                    cmdb_dep.sys_id)

    except RuntimeError as ex:
        # Log the exception and create an incident report
        ctx.logger.error('ServiceNow "%s" workflow encountered '
                         'an exception: %s' %
                         (config['workflow'], ex))
        incident = ServiceNowObject(snc, 'incident')
        create_incident_report(incident, config, ex)
        return

    chreq = ServiceNowObject(snc, 'change_request')
    open_change_request(snc, chreq, config)
    try:
        lifecycle.install_node_instances(
            graph=ctx.graph_mode(),
            node_instances=set(ctx.node_instances))
        ctx.logger.info('Building CMDB node instances')
        cmdb_add_nodes(snc, config)
        ctx.logger.info('Building CMDB node relationships')
        cmdb_add_relationships(snc, config)
        close_change_request(snc, chreq, config, True)
    except (AttributeError, RuntimeError) as ex:
        ctx.logger.error('"%s" workflow encountered an exception: %s' %
                         (config['workflow'], ex))
        create_incident_report(snc, config, ex)
        cmdb_delete_deployment(snc, config)
        close_change_request(snc, chreq, config, False)


@workflow
def uninstall(config, **_):
    '''ServiceNow wrapper workflow'''
    snc = get_servicenow_connection(config['authentication'])

    config['workflow'] = 'sn_uninstall'
    config['deployment_id'] = ctx.deployment.id
    config['blueprint_id'] = ctx.blueprint.id
    # Update the deployment_sys_id template string
    cmdb_get_manager(snc, config)

    chreq = ServiceNowObject(snc, 'change_request')
    open_change_request(snc, chreq, config)
    try:
        lifecycle.uninstall_node_instances(
            graph=ctx.graph_mode(),
            node_instances=set(ctx.node_instances))
        cmdb_remove_nodes(snc)
        cmdb_delete_deployment(snc, config)
        close_change_request(snc, chreq, config, True)
    except (AttributeError, RuntimeError) as ex:
        ctx.logger.error('"%s" workflow encountered an exception: %s' %
                         (config['workflow'], ex))
        create_incident_report(snc, config, ex)
        close_change_request(snc, chreq, config, False)
