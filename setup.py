'''Workflow module definition'''

from setuptools import setup

setup(
    name='cloudify-servicenow-plugin',
    description='A Cloudify plugin to add ServiceNow hooks''',
    version='1.3',
    author='Joshua Cornutt',
    author_email='jcornutt@gmail.com',
    packages=['plugin'],
    install_requires=[
        'cloudify-plugins-common==3.3'
    ]
)
